package com.laith.shazam.model;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;
import com.laith.shazam.util.ImageLoadUtil;


@AutoValue
public abstract class ChartTrack implements Parcelable {
    public abstract String artistName();

    public abstract String trackTitle();

    public abstract String coverArtUrl();

    public static Builder builder() {
        return new AutoValue_ChartTrack.Builder()
                .coverArtUrl(ImageLoadUtil.PLACE_HOLDER_IMAGE)
                .artistName("Unknown Title")
                .trackTitle("Unknown Track");
    }

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder artistName(String value);

        public abstract Builder trackTitle(String value);

        public abstract Builder coverArtUrl(String value);

        public abstract ChartTrack build();
    }
}



