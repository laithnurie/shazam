package com.laith.shazam;

import android.app.Application;

import com.laith.shazam.dagger.DaggerShazamComponent;
import com.laith.shazam.dagger.NetworkModule;
import com.laith.shazam.dagger.ShazamComponent;
import com.laith.shazam.network.NetworkCall;
import com.laith.shazam.network.ShazamChartService;
import com.squareup.leakcanary.LeakCanary;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ShazamApp extends Application {

    ShazamComponent shazamComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        LeakCanary.install(this);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://cdn.shazam.com/shazam/v2/en/GB/android/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ShazamChartService shazamChartService = retrofit.create(ShazamChartService.class);
        NetworkCall networkCall = new NetworkCall(shazamChartService);

        shazamComponent = DaggerShazamComponent.builder()
                .networkModule(new NetworkModule(networkCall))
                .build();
    }

    public ShazamComponent getShazamComponent() {
        return shazamComponent;
    }
}
