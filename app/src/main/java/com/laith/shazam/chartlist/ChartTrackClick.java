package com.laith.shazam.chartlist;

import com.laith.shazam.model.ChartTrack;

public interface ChartTrackClick {
    void chartItemSelected(ChartTrack chartTrack);
}
