package com.laith.shazam.chartlist;

import android.os.Bundle;

public interface ChartListViewModel {
    void onSaveInstanceState(Bundle bundle);
    void onResume();
}
