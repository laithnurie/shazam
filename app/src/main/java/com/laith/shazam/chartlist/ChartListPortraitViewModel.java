package com.laith.shazam.chartlist;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.laith.shazam.R;
import com.laith.shazam.charttrack.ChartTrackActivity;
import com.laith.shazam.model.ChartTrack;
import com.laith.shazam.network.ChartsNetworkCall;
import com.laith.shazam.network.ChartsResponseCallBack;
import com.laith.shazam.network.InternetHelper;
import com.laith.shazam.util.NotifyUtil;

import java.util.ArrayList;

public class ChartListPortraitViewModel implements ChartListViewModel, ChartsResponseCallBack,
        ChartTrackClick {

    private final RecyclerView chartsRv;
    private ChartsNetworkCall networkCall;
    private final Context context;
    private final Bundle savedInstanceState;
    private final static String CHART_TRACKS_KEY = "chartTracks";
    private ArrayList<ChartTrack> chartTracks;

    public ChartListPortraitViewModel(View rootView, Context context, Bundle savedInstanceState,
                                      ChartsNetworkCall networkCall) {
        chartsRv = (RecyclerView) rootView.findViewById(R.id.charts_list);
        chartsRv.setLayoutManager(new LinearLayoutManager(context));
        this.networkCall = networkCall;
        this.context = context;
        this.savedInstanceState = savedInstanceState;
    }

    @Override
    public void onResume() {
        if (savedInstanceState != null && savedInstanceState.getParcelableArrayList(CHART_TRACKS_KEY) != null) {
            chartTracks = savedInstanceState.getParcelableArrayList(CHART_TRACKS_KEY);
            if (chartTracks.size() > 0) {
                updateCharts(chartTracks);
            }
        } else {
            if (InternetHelper.isConnected(context)) {
                networkCall.getCharts(this);
            } else {
                NotifyUtil.toast(context, R.string.check_connection);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(CHART_TRACKS_KEY, chartTracks);
    }

    @Override
    public void onChartTracksResponse(ArrayList<ChartTrack> chartTracks) {
        this.chartTracks = chartTracks;
        if (savedInstanceState != null) {
            savedInstanceState.putParcelableArrayList(CHART_TRACKS_KEY, chartTracks);
        }
        updateCharts(chartTracks);
    }

    @Override
    public void onChartTracksError() {
        NotifyUtil.toast(context, R.string.chart_track_error_notif);
    }

    @Override
    public void chartItemSelected(ChartTrack chartTrack) {
        context.startActivity(ChartTrackActivity.getIntent(chartTrack, context));
    }

    private void updateCharts(ArrayList<ChartTrack> chartTracks) {
        chartsRv.setAdapter(new ChartsAdapter(chartTracks, context, this));
    }
}
