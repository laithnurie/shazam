package com.laith.shazam.chartlist;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.laith.shazam.R;
import com.laith.shazam.ShazamApp;
import com.laith.shazam.network.NetworkCall;

import javax.inject.Inject;

public class ChartListActivity extends AppCompatActivity {

    @Inject
    NetworkCall networkCall;
    private ChartListViewModel chartListViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart_list);
        ((ShazamApp) getApplication()).getShazamComponent().inject(this);

        // So usually I will have multiple view models for each combination of orientation and screensize
        // Depending how much they vary, all of their specific logic will be in their own ViewModel
        chartListViewModel = new ChartListPortraitViewModel(
                findViewById(android.R.id.content), this, savedInstanceState, networkCall);
    }

    @Override
    protected void onResume() {
        super.onResume();
        chartListViewModel.onResume();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        chartListViewModel.onSaveInstanceState(outState);
    }
}
