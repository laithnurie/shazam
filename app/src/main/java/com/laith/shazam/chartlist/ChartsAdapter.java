package com.laith.shazam.chartlist;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.laith.shazam.util.ImageLoadUtil;
import com.laith.shazam.R;
import com.laith.shazam.model.ChartTrack;

import java.util.ArrayList;

public class ChartsAdapter extends RecyclerView.Adapter<ChartsAdapter.ViewHolder> {

    private final ArrayList<ChartTrack> chartTracks;
    private final Context context;
    private final ChartTrackClick chartTrackClick;

    public ChartsAdapter(ArrayList<ChartTrack> chartTracks, Context context,
                         ChartTrackClick chartTrackClick) {
        this.chartTracks = chartTracks;
        this.context = context;
        this.chartTrackClick = chartTrackClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chart_row_item,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ChartTrack chartTrack = chartTracks.get(position);
        holder.txtArtistName.setText(chartTrack.artistName());
        holder.txtTrackTitle.setText(chartTrack.trackTitle());
        ImageLoadUtil.loadUserImage(chartTrack.coverArtUrl(), context, holder.imgCoverArt);
    }

    @Override
    public int getItemCount() {
        return chartTracks.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        final TextView txtArtistName;
        final TextView txtTrackTitle;
        final ImageView imgCoverArt;

        ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            txtArtistName = (TextView) v.findViewById(R.id.txt_artist_name);
            txtTrackTitle = (TextView) v.findViewById(R.id.txt_song_title);
            imgCoverArt = (ImageView) v.findViewById(R.id.img_cover_art);
        }

        @Override
        public void onClick(View view) {
            chartTrackClick.chartItemSelected(chartTracks.get(getAdapterPosition()));
        }
    }
}
