package com.laith.shazam.charttrack;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.laith.shazam.R;
import com.laith.shazam.model.ChartTrack;

public class ChartTrackActivity extends AppCompatActivity {

    private static final String CHART_TRACK_PARAM = "chartTrackParam";

    public static Intent getIntent(ChartTrack chartTrack, Context context){
        Intent chartTrackIntent = new Intent(context, ChartTrackActivity.class);
        chartTrackIntent.putExtra(CHART_TRACK_PARAM, chartTrack);
        return chartTrackIntent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart_track);
        Bundle bundle = getIntent().getExtras();

        ChartTrack chartTrack = (ChartTrack) bundle.get(CHART_TRACK_PARAM);

        ChartTrackViewModel chartTrackViewModel = new ChartTrackPortraitViewModel(findViewById(android.R.id.content));
        chartTrackViewModel.setTrack(chartTrack);
    }
}
