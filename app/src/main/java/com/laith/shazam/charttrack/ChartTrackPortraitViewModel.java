package com.laith.shazam.charttrack;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.laith.shazam.util.ImageLoadUtil;
import com.laith.shazam.R;
import com.laith.shazam.model.ChartTrack;


public class ChartTrackPortraitViewModel implements ChartTrackViewModel {
    private TextView artistName;
    private TextView songTitle;
    private ImageView coverArt;

    public ChartTrackPortraitViewModel(View rootView) {
        artistName = (TextView) rootView.findViewById(R.id.txt_artist_name);
        songTitle = (TextView) rootView.findViewById(R.id.txt_song_title);
        coverArt = (ImageView) rootView.findViewById(R.id.img_cover_art_bg);
    }

    @Override
    public void setTrack(ChartTrack chartTrack) {
        artistName.setText(chartTrack.artistName());
        songTitle.setText(chartTrack.trackTitle());
        ImageLoadUtil.loadUserImage(chartTrack.coverArtUrl(), coverArt.getContext(), coverArt);
    }
}
