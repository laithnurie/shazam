package com.laith.shazam.charttrack;


import com.laith.shazam.model.ChartTrack;

public interface ChartTrackViewModel {
    void setTrack(ChartTrack chartTrack);
}
