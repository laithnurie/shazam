package com.laith.shazam.util;

import android.content.Context;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class ImageLoadUtil {
    public static String PLACE_HOLDER_IMAGE = "https://placeholdit.imgix.net/~text?txtsize=9&bg=FFFFFF&txt=100%C3%97100&w=100&h=100";

    public static void loadUserImage(String imageUrl, Context context, ImageView imageView) {
        if (imageUrl != null && !imageUrl.isEmpty()) {
            Picasso.with(context).load(imageUrl).fit().centerCrop().into(imageView);
        } else {
            Picasso.with(context).load(PLACE_HOLDER_IMAGE).into(imageView);
        }
    }
}
