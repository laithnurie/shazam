package com.laith.shazam.util;

import android.content.Context;
import android.support.annotation.StringRes;
import android.widget.Toast;

public class NotifyUtil {
    public static void toast(Context context, @StringRes int stringResourceId) {
        Toast.makeText(context, stringResourceId, Toast.LENGTH_SHORT).show();
    }
}
