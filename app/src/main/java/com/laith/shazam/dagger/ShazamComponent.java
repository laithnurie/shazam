package com.laith.shazam.dagger;


import com.laith.shazam.chartlist.ChartListActivity;

import dagger.Component;

@PerApp
@Component(
        modules = {
                NetworkModule.class
        }
)
public interface ShazamComponent {
    void inject(ChartListActivity chartListActivity);
}
