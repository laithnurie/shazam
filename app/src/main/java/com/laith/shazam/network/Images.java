package com.laith.shazam.network;


import com.google.gson.annotations.SerializedName;

public class Images {
    @SerializedName("default")
    private String defaultImage;

    public String getDefaultImage() {
        return defaultImage;
    }
}
