package com.laith.shazam.network;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ShazamChartService {

    @GET("-/chart/recent-music/1month/-/-")
    Call<ChartsResponse> getCharts();
}
