package com.laith.shazam.network;

import com.google.gson.annotations.SerializedName;

public class ChartTrackResponse {

    @SerializedName("heading")
    private Heading heading;

    @SerializedName("images")
    private Images images;

    public Heading getHeading() {
        return heading;
    }

    public Images getImages() {
        return images;
    }
}
