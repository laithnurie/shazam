package com.laith.shazam.network;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ChartsResponse {

    @SerializedName("chart")
    private ArrayList<ChartTrackResponse> chartTrackResponses;

    public ArrayList<ChartTrackResponse> getChartList() {
        return chartTrackResponses;
    }
}
