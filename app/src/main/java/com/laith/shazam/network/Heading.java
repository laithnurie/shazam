package com.laith.shazam.network;

import com.google.gson.annotations.SerializedName;

public class Heading {
    @SerializedName("title")
    private String trackTitle;

    @SerializedName("subtitle")
    private String artistName;

    public String getTrackTitle() {
        return trackTitle;
    }

    public String getArtistName() {
        return artistName;
    }
}
