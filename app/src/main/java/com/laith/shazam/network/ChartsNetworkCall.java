package com.laith.shazam.network;


public interface ChartsNetworkCall {
    void getCharts(final ChartsResponseCallBack callBack);
}
