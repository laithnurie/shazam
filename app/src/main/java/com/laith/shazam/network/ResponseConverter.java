package com.laith.shazam.network;

import com.laith.shazam.util.ImageLoadUtil;
import com.laith.shazam.model.ChartTrack;

import java.util.ArrayList;

public class ResponseConverter {

    public static ArrayList<ChartTrack> getTracksFromResponse(ChartsResponse chartsResponse) {
        if (chartsResponse.getChartList() != null) {
            ArrayList<ChartTrack> chartTracks = new ArrayList<>();
            ChartTrack.Builder chartTrackBuilder = ChartTrack.builder();

            for (ChartTrackResponse chart : chartsResponse.getChartList()) {

                if (chart.getHeading() != null) {
                    Heading heading = chart.getHeading();

                    String artistName = heading.getArtistName() != null
                            ? heading.getArtistName() : "Unknown Artist";
                    String trackTitle = heading.getTrackTitle() != null
                            ? heading.getTrackTitle() : "Unknown Track";
                    chartTrackBuilder
                            .artistName(artistName)
                            .trackTitle(trackTitle);
                } else {
                    chartTrackBuilder
                            .artistName("Unknown Artist")
                            .trackTitle("Unknown Track");
                }

                if (chart.getImages() != null) {
                    Images images = chart.getImages();
                    String imageUrl = images.getDefaultImage() != null ? images.getDefaultImage() :
                            ImageLoadUtil.PLACE_HOLDER_IMAGE;
                    chartTrackBuilder.coverArtUrl(imageUrl);
                } else {
                    chartTrackBuilder.coverArtUrl(ImageLoadUtil.PLACE_HOLDER_IMAGE);
                }
                chartTracks.add(chartTrackBuilder.build());
            }

            return chartTracks;
        }
        return null;
    }
}
