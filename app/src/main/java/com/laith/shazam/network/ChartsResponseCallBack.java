package com.laith.shazam.network;

import com.laith.shazam.model.ChartTrack;

import java.util.ArrayList;

public interface ChartsResponseCallBack {
    void onChartTracksResponse(ArrayList<ChartTrack> chartTracks);
    void onChartTracksError();
}
