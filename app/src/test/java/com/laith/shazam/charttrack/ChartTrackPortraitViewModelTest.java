package com.laith.shazam.charttrack;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.laith.shazam.R;
import com.laith.shazam.chartlist.ChartListPortraitViewModel;
import com.laith.shazam.chartlist.ChartsAdapter;
import com.laith.shazam.model.ChartTrack;
import com.laith.shazam.network.ChartsNetworkCall;
import com.laith.shazam.network.InternetHelper;
import com.laith.shazam.util.NotifyUtil;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;

import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({InternetHelper.class, NotifyUtil.class, ChartTrackActivity.class})
public class ChartTrackPortraitViewModelTest {

    @Mock
    View rootView;

    @Mock
    Context context;

    @Mock
    ChartsNetworkCall chartsNetworkCall;

    @Mock
    Bundle savedInstanceState;

    @Mock
    RecyclerView chartsRv;

    private final static String CHART_TRACKS_KEY = "chartTracks";

    private ChartListPortraitViewModel sut;

    @Before
    public void setUp() {
        initMocks(this);
        mockStatic(InternetHelper.class);
        when(rootView.findViewById(R.id.charts_list)).thenReturn(chartsRv);
    }

    @Test
    public void whenActivityLoadedTheFirstTime() {

        when(InternetHelper.isConnected(context)).thenReturn(true);
        sut = new ChartListPortraitViewModel(rootView, context, null, chartsNetworkCall);
        sut.onResume();

        verify(chartsNetworkCall, times(1)).getCharts(sut);
        ArrayList<ChartTrack> chartTracksResponse = createChartTracks();
        sut.onChartTracksResponse(chartTracksResponse);
        verify(chartsRv, times(1)).setAdapter(isA(ChartsAdapter.class));
    }

    @Test
    public void whenActivityLoadedTheSecondTime() {
        ArrayList<ChartTrack> chartTracks = createChartTracks();
        doReturn(chartTracks).when(savedInstanceState).getParcelableArrayList(CHART_TRACKS_KEY);
        sut = new ChartListPortraitViewModel(rootView, context, savedInstanceState, chartsNetworkCall);
        sut.onResume();

        when(InternetHelper.isConnected(context)).thenReturn(true);

        verify(chartsRv, times(1)).setAdapter(isA(ChartsAdapter.class));
        verify(chartsNetworkCall, never()).getCharts(sut);
    }

    @Test
    public void whenSecondtNetworkCallFailed() {
        mockStatic(NotifyUtil.class);
        when(InternetHelper.isConnected(context)).thenReturn(true);
        sut = new ChartListPortraitViewModel(rootView, context, null, chartsNetworkCall);
        sut.onResume();

        verify(chartsNetworkCall, times(1)).getCharts(sut);
        sut.onChartTracksError();

        verifyStatic(times(1));
        NotifyUtil.toast(context, R.string.chart_track_error_notif);
    }

    @Test
    public void whenThereIsNoInternet() {
        mockStatic(NotifyUtil.class);
        when(InternetHelper.isConnected(context)).thenReturn(false);
        sut = new ChartListPortraitViewModel(rootView, context, null, chartsNetworkCall);
        sut.onResume();

        verify(chartsNetworkCall, never()).getCharts(sut);

        verifyStatic(times(1));
        NotifyUtil.toast(context, R.string.check_connection);
    }

    @Test
    public void whenChartItemSelected() {
        mockStatic(ChartTrackActivity.class);
        when(InternetHelper.isConnected(context)).thenReturn(true);

        sut = new ChartListPortraitViewModel(rootView, context, null, chartsNetworkCall);
        sut.onResume();

        ChartTrack testTrack = ChartTrack.builder().trackTitle("test").artistName("test").build();
        Intent mockedIntent = mock(Intent.class);
        when(ChartTrackActivity.getIntent(testTrack, context)).thenReturn(mockedIntent);

        sut.chartItemSelected(testTrack);
        verify(context, times(1)).startActivity(mockedIntent);
    }

    private ArrayList<ChartTrack> createChartTracks() {
        ArrayList<ChartTrack> chartTracks = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            chartTracks.add(
                    ChartTrack.builder()
                            .artistName("artist " + i)
                            .trackTitle("track " + i)
                            .coverArtUrl("https://placeholdit.imgix.net/~text?txtsize=9&bg=FFFFFF&txt=100%C3%97100&w=100&h=100")
                            .build()
            );
        }
        return chartTracks;
    }


}